#include <iostream>
#include <stdexcept>  // for std::runtime_error
#include <string>

/**
 * \brief Generates a target word for a game of Text Hangman.
 *
 * \return  The target word.
 */
std::string generate_target_word()
{
  // TODO: Return a random target word from the words.txt file.
  return "wibble";
}

/**
 * \brief Renders the hangman figure associated with the specified score.
 *
 * \param score A score in a game of Text Hangman.
 */
void render_hangman(int score)
{
  // TODO: Render the hangman figure here.
  if(score > 0) std::cout << "Score: " << score << "\n\n";
  else std::cout << "Game Over!\n\n";
}

int main()
try
{
  std::string target = generate_target_word();
  std::string current(target.size(), '_');
  int score = 7;

  // TODO: Implement the main game loop.

  // At the end of the game, let the player know the final result.
  if(current == target)
  {
    std::cout << "Congratulations!\n";
  }
  else
  {
    render_hangman(0);
    std::cout << "Target Word: " << target << '\n';
  }

  return 0;
}
catch(std::exception& e)
{
  std::cerr << e.what() << '\n';
}
